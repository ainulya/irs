angular.module('starter.services', [])
.constant('API', {
  url:'http://kasaucup.co.id/dev/assets/grantella/rs_ci/apis/', //URL API
  config : {headers: {'Authorization': localStorage.getItem('TokenKey')  }},
  notAuthenticated: 'auth-not-authenticated'
})
// http://kasaucup.co.id/dev/assets/grantella/rs_ci/apis/pasien/save 
.factory('AuthInterceptor', function ($rootScope, $q, API) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: API.notAuthenticated,
      }[response.status], response);
      return $q.reject(response);
    }
  };
})

.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
})

.service('AuthService', function($q, $http, API, Service) {
  var LOCAL_TOKEN_KEY = 'TokenKey';
  var isAuthenticated = false;
  var authToken;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {useCredentials(JSON.parse(token));}
  }
  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, JSON.stringify(token));
    useCredentials(token);
  }

  function useCredentials(token) {// Set the token as header for your requests!
    isAuthenticated = true;
    $http.defaults.headers.common.Authorization = token.api_key;
  }

  function destroyUserCredentials() {// delete $http.defaults.headers.common.Authorization;
    isAuthenticated = false;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }

  var daftar = function(user) {
    return $q(function(resolve, reject) {
      $http.post(API.url + 'register', user).then(function(result) {
        if (!result.data.status) {reject(result.data.message);}
        else { resolve(result.data.message);}
      });
    });
  };

  var login = function(user) {
    return $q(function(resolve, reject) {
      $http.post(API.url+'auth/login', user)
      .then(function(result) {
        if (result.data.status) {
            storeUserCredentials({api_key:result.data.data.api_key,role:result.data.role.role});
            resolve({api_key:result.data.data.api_key,role:result.data.role.role});
        } else {
            reject(Service.decodeURL(result.data.message));
        }
      });
    });
  };

  var logout = function() {
    destroyUserCredentials();
  };
  var apiKey = function(){
    var profile = JSON.parse(localStorage.getItem(LOCAL_TOKEN_KEY));
    return profile.api_key
  };
  var role = function(){
    var profile = JSON.parse(localStorage.getItem(LOCAL_TOKEN_KEY));
    return profile.role
  };

  loadUserCredentials();
  return {
    login: login,
    daftar: daftar,
    logout: logout,
    role: role,
    apiKey: apiKey,
    loadUserCredentials: loadUserCredentials,
    isAuthenticated: function() {return isAuthenticated;},
  };
})

.service('DataService', function($http, API, $q, Service, AuthService,CacheFactory, Service) {
    // var options = {        deleteOnExpire: 'aggressive',        recycleFreq: 5000      }
    // if (!CacheFactory.get('dataCache')) {      CacheFactory.createCache('dataCache', options)    }
    // var dataCache = CacheFactory.get('dataCache');
    var getData = function(q){
      return $q(function(resolve, reject) {
              $http.get(API.url +q)
                .success(function (result) {
                    // dataCache.put(q, result);
                    resolve(result);
                });
      });
            
    }
    var postData = function(q,data) {
      return $q(function(resolve, reject) {
              $http.post(API.url+q, data).then(function(result) {
                  if (result.data.status){ resolve(result.data.data);}
                  else{ reject(result.data.message); }
              });
      });
    };
    return {
        getData: getData,
        postData:postData
    };
})

.factory('Service', function($http, $window, $ionicActionSheet,$rootScope, $timeout,$cordovaPush, $ionicLoading,$ionicModal, $ionicPopup, $ImageCacheFactory, $q,$cordovaToast, $cordovaCamera,$cordovaGeolocation) {

    var imageCache = function(data){//Build the list of images to preload
        var images = [];
        for(var i=0; i<data.length;i++){
            images.push(data[i]);
        }
         $ImageCacheFactory.Cache(images).then(function(){
                return console.log("Images done loading!");
            },function(failed){
                return console.log("An image filed: "+failed);
            });
    }
    var loading = function(durasi) {
        return  $ionicLoading.show({
                template: '<ion-spinner id="loading" icon="spiral" class="spinner-balanced"></ion-spinner>',
                showBackdrop: true,
                duration: durasi
                });
    }
    var loadingHide = function(){
        return $ionicLoading.hide();
    }
    var popupAlert = function(title, text) {
        return  $ionicPopup.alert({
                title: '<h5 class="font-11 font-bolder item-text-wrap">'+title+'</h5>',
                okType:'button-clear no-padding no-margin button-positive',
                okText:'<b>OK, Saya mengerti</b>',
                cssClass:'my-popup',
                template :  '<div class="text-center">' +
                            '<i class="no-padding ion-android-alert assertive"></i>' +
                            '<h5 class="font-11 item-text-wrap">'+text+'</h5>' +
                            '</div>'
                });
    }
    var popupConfirm = function(title,text,ok,batal) {
        return  $ionicPopup.confirm({
                title: '<h5 class="font-11 font-bolder item-text-wrap">'+title+'</h5>',
                cancelType:'button-clear button-positive letter-spacing',
                cancelText: '<b>'+batal+'</b>',
                okType:'button-clear button-dark letter-spacing',
                okText:'<b>'+ok+'</b>',
                cssClass:'my-popup',
                template :  '<div class="text-center padding-horizontal">' +
                            '<h4 class="item-text-wrap">'+text+'</h4>' +
                            '</div>'
                })
    }

    var modalImage = function(image){
        return $ionicModal.fromTemplate('<div class="modal image-modal transparent" on-swipe-down="closeModal()">'+
                                        '<ion-slide-box on-slide-changed="slideChanged(index)" active-slide="activeSlide">'+
                                        '<ion-slide ng-repeat="image in allImages">'+
                                        '<ion-scroll direction="xy" scrollbar-x="false" scrollbar-y="false"'+
                                        'zooming="true" min-zoom="1" style="width: 100%; height: 100%"'+
                                        'delegate-handle="scrollHandle{{$index}}" on-scroll="updateSlideStatus(activeSlide)" on-release="updateSlideStatus(activeSlide)">'+
                                        '<div class="image" style="background-image: url( img/'+ image +')"></div>'+
                                        '</ion-scroll>'+
                                        '</ion-slide>'+
                                        '</ion-slide-box>'+
                                        '</div>', {
                scope: $scope
                }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.show();
                });
    }
    return {
        loading: loading,
        loadingHide:loadingHide,
        popupAlert: popupAlert,
        popupConfirm:popupConfirm,
        imageCache:imageCache
    };
})

