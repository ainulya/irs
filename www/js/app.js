angular.module('starter', ['ionic','ngCordova','ionic-modal-select', 'chart.js','onezone-datepicker','ionic-letter-avatar','ngAnimate', 'angular-cache','ionic.ion.imageCacheFactory','starter.controllers','starter.services','starter.directives'])

.run(function($rootScope, $http,$state, AuthService,DataService, API,$ionicPlatform,$cordovaSplashscreen,$cordovaStatusbar,CacheFactory,Service) {
  $ionicPlatform.ready(function() {
    $cordovaSplashscreen.hide()
    if(AuthService.isAuthenticated()){
    AuthService.loadUserCredentials();
    }
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(false);
    }
    if (window.StatusBar) {
      $cordovaStatusbar.overlaysWebView(true);
      $cordovaStatusbar.styleHex('#FF0000');
    }
  });

})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, ChartJsProvider) {
$ionicConfigProvider.backButton.icon('ion-android-arrow-back');
$ionicConfigProvider.backButton.text('');
$ionicConfigProvider.scrolling.jsScrolling(false);
$ionicConfigProvider.navBar.alignTitle('center');
$ionicConfigProvider.spinner.icon('spiral');
$ionicConfigProvider.views.transition('none');
$ionicConfigProvider.form.toggle('ios');
$ionicConfigProvider.views.maxCache(0);

ChartJsProvider.setOptions({
      showLines: true,
      responsive: true,
      scales: {
        yAxes: [{
          display: true,
          gridLines : { display : false }
        }],
        xAxes: [{
          display: true,
          gridLines : {display : false}
        }]
      }
});

//////////////////////////// STATE
  $stateProvider
  .state('login', {
    url:'/login',
    templateUrl:'templates/login.html',
    controller: 'LoginCtrl'
  })
  .state('daftar', {
    url:'/daftar',
    templateUrl:'templates/daftar.html',
    controller: 'LoginCtrl'
  })
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl',
    onEnter:function($state,AuthService){
      if(!AuthService.isAuthenticated()){
        $state.go("login");
      }
    }
  })

  .state('app.dash', {
    url: '/dash',
    views: {
      'menuContent': {
        templateUrl: 'templates/dash.html',
        controller: 'DashCtrl'
      }
    }
  })
  .state('app.pasien', {
    url: '/pasien',
    views: {
      'menuContent': {
        templateUrl: 'templates/pasien.html',
        controller: 'PasienCtrl'
      }
    }
  })
  .state('app.detail-pasien', {
    url: '/detail-pasien/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/detail-pasien.html',
        controller: 'DetailPasienCtrl'
      }
    }
  })
  .state('app.monitoring', {
    url: '/monitoring',
    views: {
      'menuContent': {
        templateUrl: 'templates/monitoring.html',
        controller: 'MonitoringCtrl'
      }
    }
  })
  .state('app.monitoring-add', {
    url: '/monitoring/:id/:monitoring',
    views: {
      'menuContent': {
        templateUrl: 'templates/monitoring-add.html',
        controller: 'MonitoringCtrl'
      }
    }
  })
  .state('app.monitoring-add2', {
    url: '/monitoring/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/monitoring-add2.html',
        controller: 'MonitoringCtrl'
      }
    }
  })
  .state('app.monitoring-detail', {
    url: '/monitoring-detail/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/monitoring-detail.html',
        controller: 'MonitoringDetailCtrl'
      }
    }
  })
  .state('app.detail-grafik', {
    url: '/detail-grafik/:data',
    views: {
      'menuContent': {
        templateUrl: 'templates/detail-grafik.html',
        controller: 'DetailGrafikCtrl'
      }
    }
  })
  .state('app.laporan', {
    url: '/laporan',
    views: {
      'menuContent': {
        templateUrl: 'templates/laporan.html',
        controller: 'LaporanCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('app/dash');
});
