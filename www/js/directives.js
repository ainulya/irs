angular.module('starter.directives', [])
.directive('elastic', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
    }
])
.directive('groupedRadio', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      model: '=ngModel',
      value: '=groupedRadio'
    },
    link: function(scope, element, attrs, ngModelCtrl) {
      element.addClass('button');
      element.on('click', function(e) {
        scope.$apply(function() {
          ngModelCtrl.$setViewValue(scope.value);
        });
      });

      scope.$watch('model', function(newVal) {
        element.removeClass('button-assertive');
        if (newVal === scope.value) {
          element.addClass('button-assertive');
        }
      });
    }
  };
})
.directive('backgroundImage', function(){
	return function(scope, element, attrs){
	// restrict:'A',
		attrs.$observe('backgroundImage', function(value) {
			element.css({
				'background-image': 'url(' + value +')',
                'background-size':'cover',
                'background-position': 'center center'
			});
		});
	};
})
.filter('dateToISO', function() {
  return function(input) {
    input = new Date(input);
    return input;
  };
})
.filter('elapsed', function(){
    return function(date){
        if (!date) return;
        var time = Date.parse(date),
            timeNow = new Date().getTime(),
            difference = timeNow - time,
            seconds = Math.floor(difference / 1000),
            minutes = Math.floor(seconds / 60),
            hours = Math.floor(minutes / 60),
            days = Math.floor(hours / 24);
        if ( minutes < 1 ) {
            return "beberapa detik yang lalu";
        } else if (minutes < 60) {
            return minutes + " menit yang lalu";
        } else if (hours < 24) {
            return hours + " jam yang lalu";
        } else {
            return date;
        }
    }
})
.filter('waktu', function(){
    return function(date){
        if (!date) return;
        var time = Date.parse(date),
            timeNow = new Date().getTime();
        if ( timeNow < 1 ) {
            return "beberapa detik yang lalu";
        } else if (minutes < 60) {
            return minutes + " menit yang lalu";
        } else if (hours < 24) {
            return hours + " jam yang lalu";
        } else {
            return date;
        }
    }
})
.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';
            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;
            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                  if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }
            return value + (tail || ' …');
        };
})
