angular.module('starter.controllers', [])
.filter('slash',function(){
    return function(input){
        return input.replace('.','/');
    }
})
.controller('AppCtrl', function($scope, $state,$rootScope,AuthService, Service, DataService,API) {
  $scope.logout = function(){
    AuthService.logout();
    $state.go('login');
  }
  $scope.role = AuthService.role();
  console.log($scope.role)

})
.controller('LoginCtrl', function($state,$scope,AuthService,Service) {
   $scope.loginData = {}
   $scope.doLogin = function(data){
     Service.loading();
    AuthService.login(data).then(function(msg) {
      Service.loadingHide();
        $state.go('app.dash');
    },function(err){
      Service.loadingHide();
      Service.popupAlert('Login Gagal',err);
    });
   }
})
.controller('DashCtrl', function($state,$ionicHistory,$scope,DataService) {
    DataService.getData('pasien/pasien').then(function(res){
      console.log(res);
      $scope.pasien = res.data;
    },function(err){
      Service.popupAlert('Terjadi Kesalahan', err);
    });
})
.controller('PasienCtrl',function($scope,$state,$rootScope,DataService,$stateParams,$ionicSlideBoxDelegate,$ionicPopup){
  $scope.back = function() { $ionicHistory.goBack();}  
  $scope.data = {};
  $scope.pickDate = function(){
    $ionicPopup.confirm({
      template: '<div ng-controller="popupCtrl"><onezone-datepicker datepicker-object="onezoneDatepicker"></onezone-datepicker></div>',
      title: 'Pilih Tanggal',cssClass:'pick-date',
      cancelType:'button-clear button-assertive letter-spacing',cancelText: '<b>Batal</b>',
      okType:'button-clear button-dark letter-spacing',okText:'<b>Set</b>',
      }).then(function(res) {
        if(res) {
          $scope.data.tgl_lahir = $rootScope.tanggal;
          $scope.data.umur = parseInt(calcAge($scope.data.tgl_lahir));
        }
    });
  }

  $scope.id = $stateParams.id;
  $scope.doPasien = function(){
    $scope.data.umur = parseInt(calcAge($scope.data.tgl_lahir));
    DataService.postData('/pasien/save',$scope.data).then(function(res){
      $state.go('app.monitoring-add2',{id:res.id});
    },function(err){
      Service.popupAlert('Terjadi Kesalahan', err);
    });
  }
  function calcAge(dateString) {
  var birthday = +new Date(dateString);
  return ((Date.now() - birthday) / (31557600000));
  }
})
.controller('DetailPasienCtrl',function($scope,$state,$rootScope,DataService,$stateParams,$ionicSlideBoxDelegate,$ionicPopup){
  $scope.data = {};
  DataService.getData('pasien/pasien_detail/'+$stateParams.id).then(function(data){
    $scope.data = data.pasien[0];
  });

})
.controller('popupCtrl',function($scope,$filter,$rootScope){
  $scope.onezoneDatepicker = {
      months: ["Januari","Fabruari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
      daysOfTheWeek: ["Mi","Sen","Sel","Rab","Kam","Jum","Sab"],
      date: new Date,    mondayFirst: false,disablePastDays: false,    disableSwipe: true,    disableWeekend: false,    showDatepicker: true,showTodayButton: false,    calendarMode: true,    hideCancelButton: true,    hideSetButton: true,
      callback: function(value){$rootScope.tanggal = $filter('date')(value, "yyyy/MM/dd"); }
  };
})
.controller('LaporanCtrl', function($stateParams,$scope,DataService, $state,$ionicHistory, Service) {
      // $scope.name = $stateParams.name;
      // $scope.kode = $stateParams.kode;
      // $scope.menu = function(){
      //   $ionicHistory.nextViewOptions({disableBack: true, disableAnimate: true});/// disable back button
      //   $state.go('app.beranda');
      // };
  DataService.getData('pasien/pasien').then(function(res){
    $scope.pasien = res.data;
  });
})
.controller('DetailGrafikCtrl', function($stateParams,$scope) {
     $scope.list = JSON.parse($stateParams.data);
})
.controller('MonitoringCtrl', function(DataService,$filter,$ionicSlideBoxDelegate,$scope, $state,$ionicHistory, $stateParams, Service) {
  $scope.$on('$ionicView.beforeEnter', function(){
    DataService.getData('pasien/pasien').then(function(res){
      $scope.pasien = res.data;
    });
    DataService.getData('pasien/penyakit').then(function(res){
      $scope.penyakit = res.data;
    });
    $scope.inputs = [{nama:null,aturan: null    }];
    $scope.diag = [];  
    $scope.monitoring = $stateParams.monitoring;
    $scope.data = {};
    $scope.data.id_pasien = $stateParams.id;
    $scope.data.denyut_nadi = $stateParams.denyut;
    $scope.data.suhu_tubuh = $stateParams.suhu;
    $scope.data.pernafasan = $stateParams.pernapasan;
    $scope.data.tekanan_darah1 = $stateParams.tekanan1;
    $scope.data.tekanan_darah2 = $stateParams.tekanan2;
    $scope.data.waktu = getWaktu();
});
   function getWaktu(){
     var timeNow = new Date().getHours();
           if(timeNow>=4 &&timeNow<=10){  return 'pagi'}
           else if(timeNow>=11 &&timeNow<=15){ return 'siang';}
           else if(timeNow>=16 &&timeNow<=18){ return 'sore';}
           else{  return 'malam';}
   }
  
  $scope.addInput = function () { $scope.inputs.push({nama:null,aturan: null    });   };
  $scope.removeInput = function (index) { $scope.inputs.splice(index, 1); };
  $scope.addDiag = function(newValue, oldValue){   $scope.diag.push({kode:newValue.kode,nama:newValue.diagnosa})};
  $scope.delDiag = function (index) { $scope.diag.splice(index, 1); };

  $scope.addDiagnosa = function(){
    Service.loading();
console.log({id_pasien:$stateParams.id,obat:$scope.inputs,diagnosa:$scope.diag});
    DataService.postData('pasien/tanda_vital',{id_pasien:$stateParams.id,obat:$scope.inputs,diagnosa:$scope.diag}).then(function(data){
      console.log(data);
      Service.loadingHide();
      Service.popupAlert('Sukses','Data entri sukses');
      $state.go('app.monitoring-add',{id:$stateParams.id,monitoring:true});          
    });
  }

  $scope.doSubmit = function(){
    $scope.data.obat = JSON.parse($stateParams.obat);
    $scope.data.diagnosa = JSON.parse($stateParams.diag);
    console.log($scope.data);
    Service.loading();
    DataService.postData('pasien/tanda_vital',$scope.data).then(function(data){
      console.log(data);
      Service.loadingHide();
      Service.popupAlert('Sukses','Data entri sukses');
      $ionicHistory.nextViewOptions({disableBack: true, disableAnimate: true});/// disable back button
      $state.go('app.monitoring-add');
    });
  }
  $scope.doMonitor = function(suhu, denyut, tekanan1,tekanan2,pernafasan){
    var datas = {id_pasien: $stateParams.id,waktu: getWaktu(),denyut_nadi:denyut,pernafasan: pernafasan,tekanan_darah1: tekanan1, tekanan_darah2:tekanan2,suhu_tubuh: suhu};
    console.log(datas);
    Service.loading();
    DataService.postData('pasien/monitoring',datas).then(function(data){
      Service.loadingHide();
      Service.popupAlert('Sukses','Data entri sukses');
      $ionicHistory.nextViewOptions({disableBack: true, disableAnimate: true});/// disable back button
      $state.go('app.monitoring');
    });
  }
})

.controller('MonitoringDetailCtrl', function(DataService,$stateParams,$scope, $state,$ionicHistory, Service) {
       
  $scope.labelChartLine = [];
  // $scope.datasetOverride1 = {hoverBackgroundColor: ["#009689", "#e74c3c"], 
  //                            hoverBorderColor: ["#009689", "#e74c3c"]};
  // $scope.datasetOverride2 = [{     borderWidth: 3,      type: 'bar'    },
  //                            {     borderWidth: 3,      type: 'line'   }];
  $scope.pasien;
    $scope.pernafasan = [];
    $scope.suhu = [];
    $scope.tekanan = [];
    $scope.denyut = [];
    $scope.labelPernafasan = [];
    $scope.labelSuhu = [];
    $scope.labelTekanan = [];
    $scope.labelDenyut = [];
  DataService.getData('pasien/pasien_detail/'+$stateParams.id).then(function(data){
    $scope.datas = data;
    $scope.pasien = data.pasien[0];
    $scope.obats = data.obat;
    $scope.pasien.umur = parseInt(calcAge($scope.pasien.tgl_lahir));
    console.log(data)
    angular.forEach(data.pernafasan, function(value, key){
     $scope.pernafasan.push(value.value);
     $scope.labelPernafasan.push(value.waktu);
    });
    angular.forEach(data.suhu_tubuh, function(value, key){
     $scope.suhu.push(value.value);
     $scope.labelSuhu.push(value.waktu);
    });
    angular.forEach(data.tekanan_darah, function(value, key){
     $scope.tekanan.push(value.value);
     $scope.labelTekanan.push(value.waktu);
    });
    angular.forEach(data.denyut_nadi, function(value, key){
     $scope.denyut.push(value.value);
     $scope.labelDenyut.push(value.waktu);
    });

  });
  DataService.getData('pasien/penyakit').then(function(res){
      $scope.penyakit = res.data;
  });
function calcAge(dateString) {
  var birthday = +new Date(dateString);
  return ((Date.now() - birthday) / (31557600000));
  }

  $scope.detailGrafik = function(data){
    console.log($scope.datas[data]);
    $state.go('app.detail-grafik',{data:JSON.stringify($scope.datas[data])});
  }
});



// var backbutton =0;
//   $ionicPlatform.registerBackButtonAction(function () {
//     if(backbutton==0){
//       backbutton++;
//       window.plugins.toast.showShortCenter('Tekan sekali lagi untuk keluar aplikasi');
//       $timeout(function(){backbutton=0;},3000);
//     }else{
//       navigator.app.exitApp();
//     }
//   }, 100);
// google.maps.event.addListenerOnce($scope.map, 'idle', function(){  loadMarkers();    });
  // function loadMarkers(){
  //       var icon = {
  //           url: "http://163.53.192.162/bengkel/files/thumbnail/"+$scope.file_name,
  //           size: new google.maps.Size(71, 71),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(0, 0),scaledSize: new google.maps.Size(35, 35)
  //       };
  //       var markerPos = new google.maps.LatLng($scope.latitude, $scope.longitude);
  //       var marker = new google.maps.Marker({map: $scope.map, position: markerPos,icon: icon});
  //       var infoWindow = new google.maps.InfoWindow({content: "<h4>" + records[i].name + "</h4>"});
  //
  //  }
